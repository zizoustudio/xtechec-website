function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("myOverlay").style.display = "block";
}

function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("myOverlay").style.display = "none";
}

// function myScroll() {
//   if (document.documentElement.scrollTop > 25) {
//     document.getElementById("menu").style.position = "fixed";
//     document.getElementById("menu").style.top = "0";
//   } else {
//     document.getElementById("menu").style.position = "relative";
//     document.getElementById("menu").style.top = "0";
//   }
//   if (document.documentElement.scrollTop > 50) {
//     document.getElementById("menu").style.position = "fixed";
//     document.getElementById("mnavbar").style.top = "0";
//   } else {
//     document.getElementById("mnavbar").style.position = "relative";
//     document.getElementById("mnavbar").style.top = "0";
//   }
//   if (document.documentElement.scrollTop > 300) {
//     document.getElementById("lnavbar").style.position = "fixed";
//     document.getElementById("lnavbar").style.top = "0";
//   } else {
//     document.getElementById("lnavbar").style.position = "relative";
//     document.getElementById("lnavbar").style.top = "0";
//   }
// }
// Función para manejar el evento cuando el ancho del viewport supera los 800px
function handleViewportWidthChange() {
  if (window.innerWidth > 800) {
      // Aquí puedes realizar las acciones que deseas cuando el ancho del viewport supera los 800px
      w3_close();
  }
}

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var y = document.getElementsByClassName("mySlideTitles");
  var z = document.getElementsByClassName("mySlideTags");
  var ym = document.getElementsByClassName("m-mySlideTitles");
  var zm = document.getElementsByClassName("m-mySlideTags");
  var ys = document.getElementsByClassName("s-mySlideTitles");
  var zs = document.getElementsByClassName("s-mySlideTags");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = x.length;
  }
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  for (i = 0; i < y.length; i++) {
    y[i].style.display = "none";
  }
  for (i = 0; i < z.length; i++) {
    z[i].style.display = "none";
  }
  for (i = 0; i < ym.length; i++) {
    ym[i].style.display = "none";
  }
  for (i = 0; i < zm.length; i++) {
    zm[i].style.display = "none";
  }
  for (i = 0; i < ym.length; i++) {
    ys[i].style.display = "none";
  }
  for (i = 0; i < zm.length; i++) {
    zs[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex - 1].style.display = "block";
  y[slideIndex - 1].style.display = "block";
  z[slideIndex - 1].style.display = "block";
  ym[slideIndex - 1].style.display = "block";
  zm[slideIndex - 1].style.display = "block";
  ys[slideIndex - 1].style.display = "block";
  zs[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " w3-white";
}

function autoSlide() {
  autoSlideInterval = setInterval(function () {
    plusDivs(1);
  }, 10000); // Cambia de imagen cada 3 segundos (ajusta el intervalo según tus necesidades)
  document.getElementById("toggleBtn").innerHTML = "&#10074;&#10074;"; // Establece el botón de pausa
}

function toggleAutoSlide() {
  if (autoSlideInterval) {
    clearInterval(autoSlideInterval);
    autoSlideInterval = null;
    document.getElementById("toggleBtn").innerHTML = "&#9658;"; // Establece el botón de reproducción
  } else {
    autoSlide();
  }
}